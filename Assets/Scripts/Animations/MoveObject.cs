﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves an Object towards an EndPosition and then wraps it back to the StatPosition
/// Only move Parent, Cloud Image or EndPos in Unity
/// </summary>
public class MoveObject : MonoBehaviour
{
    #region public variables
    public float moveSpeed = .1f;
    #endregion public variables 

    #region private variables
    RectTransform startPos,endPos,imageTransform;
    #endregion private variables

    #region monodevelop
    void Start()
	{
		startPos = transform.GetChild(0).Find("StartPos").GetComponent<RectTransform>();
		endPos = transform.GetChild(0).Find("EndPos").GetComponent<RectTransform>();

		imageTransform = transform.GetChild(1).GetComponent<RectTransform>();
	}

	void Update()
	{
		MoveTowards();
	}
    #endregion monodevelop

    #region private methods
    void MoveTowards()
	{
		if (imageTransform.localPosition == endPos.localPosition)
		{
			imageTransform.localPosition = startPos.localPosition;
		}

		imageTransform.localPosition = Vector2.MoveTowards(imageTransform.localPosition, endPos.localPosition, moveSpeed);
	}
    #endregion private methods
}
