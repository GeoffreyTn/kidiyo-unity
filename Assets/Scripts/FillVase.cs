﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Unused class that fills the vase object with flowers
/// </summary>
public class FillVase : MonoBehaviour
{
    [SerializeField]
    GameObject RealVase;

    /// <summary>
    /// Checks wether there are enough flowers in the vase to change the sprite
    /// </summary>
    public void CheckFlowers()
    {
        print("checking");
        int i = 0;
        foreach (Transform child in transform)
        {
            if (child.gameObject.name == "Flower")
                i++;
        }
        if (i >= 2)
        {
            RealVase.GetComponent<Image>().sprite = Resources.Load("Sprites/InteractiveStory/Level 2/Objects/Vase/vase02") as Sprite;
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        print("hey");
        CheckFlowers();
    }
}
