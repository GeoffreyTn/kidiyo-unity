﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class that handles unlocking presents
/// </summary>
public class CollectPresent : MonoBehaviour {

    #region public variables
    public PresentType PresentToBeCollected;
    #endregion public variables

    #region public methods
    /// <summary>
    /// Makes sure the GameObject has the same sprite as the PresentType scriptable Game Object
    /// </summary>
    public void OnEnable()
    {
        ReceivePresent();

        if (GetComponent<Image>() != null)
            GetComponent<Image>().sprite = PresentToBeCollected.Sprite;
    }

    /// <summary>
    /// Unlocks the given PresentType
    /// </summary>
    public void ReceivePresent()
    {
        PresentToBeCollected.IsUnlocked = true;
    }
    #endregion public methods
}
