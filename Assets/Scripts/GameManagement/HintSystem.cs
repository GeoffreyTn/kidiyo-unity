﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Plays a hint on what the child is supposed to do in this part of the chapter
/// </summary>
public class HintSystem : MonoBehaviour
{
    /// <summary>
    /// The audioClip thats supposed to be played at the currentEvent index
    /// </summary>
    public AudioClip[] HintsForChapterPart;

    public void PlayHint()
    {
        StoryProgressionManager s = FindObjectOfType<StoryProgressionManager>();
        if (s.CurrentEvent<HintsForChapterPart.Length)
            SoundManager.PlaySound(HintsForChapterPart[s.CurrentEvent]);
    }
}
