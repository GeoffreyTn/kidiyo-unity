﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script for interacting with objects in order
/// </summary>
public class InteractInOrder : MonoBehaviour
{
    [Header("Please place in order")]
    public GameObject[] OrderObjects, ObjectsToToggle;
    private int currentObject;
    private float WaitTime = 1f;

    /// <summary>
    /// Check if the interacted object was first in list
    /// If this is true advance the currentObject list to enable check which object is next in list
    /// </summary>
    public void CheckInteractedObjects(GameObject obj, OrderObjects.InteractEvents eventType)
    {
        //Choose from Interact event what should happen to an interacted object
        if (obj == OrderObjects[currentObject] && currentObject<OrderObjects.Length)
        {
            currentObject++;
            switch (eventType)
            {
                case global::OrderObjects.InteractEvents.Destroy:
                    Destroy(obj);
                    break;
                case global::OrderObjects.InteractEvents.Disable:
                    obj.SetActive(false);
                    break;
                case global::OrderObjects.InteractEvents.ChangeColorToRandom:
                    obj.GetComponent<Image>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
                    break;
                default:
                    break;
            }

            //Play the soundeffect attached to the interacted object
            if (obj.GetComponent<OrderObjects>().CorrectSound!=null)
                SoundManager.PlaySound(obj.GetComponent<OrderObjects>().CorrectSound);

            //If the last object has been interacted with Continue with the story
            if (currentObject >= OrderObjects.Length)
            {
                Invoke("ContinueStory", WaitTime);
            }
        }
        else
        {
            //Play the sound for when the incorrect object has been clicked
            SoundManager.PlaySound(obj.GetComponent<OrderObjects>().InCorrectSound);
        }
    }

    private void ContinueStory()
    {
        foreach (var item in ObjectsToToggle)
        {
            //toggles item activation
            item.SetActive(!item.activeInHierarchy);
        }
        StoryProgressionManager s = FindObjectOfType<StoryProgressionManager>();
        s.AdvanceStory();
    }
}
