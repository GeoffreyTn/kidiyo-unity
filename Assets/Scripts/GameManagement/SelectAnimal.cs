﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Used to select an Animal, and save the selection.
/// This is to be placed on a button or other interactable object
/// </summary>
public class SelectAnimal : MonoBehaviour {

    #region public variables
    /// <summary>
    /// List of all AnimalTypes
    /// </summary>
    public enum AnimalTypes
    {
        Butterfly, Peacock, Whale, Chameleon, Frog, Snail, Crab, Bee
    }

    [Header("The type of animal that will be selected")]
    public AnimalTypes AnimalType;

    public AnimalType BuddyType;
    #endregion public variables

    #region public methods
    /// <summary>
    /// Select an Buddy
    /// </summary>
    public void SelectBuddy()
    {
        var gdm = GameDataManger.gameDataManger;
        var ggv = GlobalGameVariables.Instance;

        gdm.AnimalType = BuddyType.name; //should be the name and not animalname

        AnimalType[] types = Resources.LoadAll<AnimalType>("Scriptable Objects/Buddies");
        foreach (var type in types)
        {
            if (type.name == gdm.AnimalType)
                ggv.AnimalType = type;
        }
    }

    /// <summary>
    /// Saves the chosen animal.
    /// </summary>
    public void ConfirmBuddySelection()
    {
        var gdm = GameDataManger.gameDataManger;
        gdm.Save();
    }
    #endregion public methods
}
