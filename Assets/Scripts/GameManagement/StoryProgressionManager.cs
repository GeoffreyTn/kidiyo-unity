﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that handles the Progression of the story.
/// </summary>
public class StoryProgressionManager : MonoBehaviour {

    #region events and delegates
    /// <summary>
    /// Delegate for StoryProgression event
    /// </summary>
    public delegate void StoryProgressionAction();
    /// <summary>
    /// Event that is called when the story is being advanced by the player.
    /// Keep in mind that you cant subscribe methods to this event that require parameters
    /// and/or need a return type
    /// because the delegate doesnt have any parameters and is a void
    /// </summary>
    public static event StoryProgressionAction ProgressStory;
    #endregion events and delegates

    #region public variables
    public bool PlayEventOnStart;
    /// <summary>
    /// Number of the current event the chapter is on. I.E. 1 is the introduction, 2 is going to the wood etc.
    /// </summary>
    public int CurrentEvent = 0;
    [HideInInspector]
    public int MaxEvents;
    /// <summary>
    /// Array of objects that contain the AudioClips for the part of the story at the index of CurrentEvent
    /// </summary>
    public GameObject[] EventObjects;
    #endregion public variables

    #region monodevelop
    #region setup event
    /// <summary>
    /// Here you add methods as subscribers to events, so when this event is called
    /// these methods are called as well.
    /// </summary>
    private void OnEnable()
    {
        ProgressStory += PlayEventSound;
        ProgressStory += IncreaseEventNumber;

        ProgressStory += BuddyBehaviour.HappyForSucces;
        ProgressStory += BuddyBehaviour.CheckForInteractables;

        ProgressStory += Tommy.ResetIdle;
    }

    /// <summary>
    /// Remove the subscribed methods from the event when this object is disabled.
    /// To prevent memory leaks and stuff like that.
    /// </summary>
    private void OnDisable()
    {
        ProgressStory -= PlayEventSound;
        ProgressStory -= IncreaseEventNumber;

        ProgressStory -= BuddyBehaviour.HappyForSucces;
        ProgressStory -= BuddyBehaviour.CheckForInteractables;

        ProgressStory -= Tommy.ResetIdle;
    }
    #endregion setup event

    private void Start()
    {
        MaxEvents = EventObjects.Length;

        if (PlayEventOnStart)
        {
            AdvanceStory();
        }
    }
    #endregion monodevelop

    #region public methods
    /// <summary>
    /// This method will be called whenever the player does something that advanced the story.
    /// It calls the event for this and with that all methods that should be called when 
    /// something happens in the story
    /// </summary>
    public void AdvanceStory()
    {
        if (ProgressStory != null)
        {
            ProgressStory();
        }
    }
    #endregion public methods

    #region private methods
    private void IncreaseEventNumber()
    {
        if (CurrentEvent < MaxEvents)
            CurrentEvent++;
        else
            EndChapter();

        if (CurrentEvent == MaxEvents)
            EndChapter();
    }

    private void PlayEventSound()
    {
        if (CurrentEvent < MaxEvents)
            EventObjects[CurrentEvent].GetComponent<SoundSequence>().StartDialogue();
    }

    /// <summary>
    /// Ends the Chapter by setting the data completed bool to true
    /// Awarding the player a Chapter Complete price
    /// Saving and returning to the title
    /// </summary>
    private void EndChapter()
    {
        var g = GameDataManger.gameDataManger;
        var global = GlobalGameMethods.Instance;

        //Make sure that the player can interact on the next screens
        GlobalGameVariables.Instance.CanInteract = true;

        g.Chapters[g.CurrentchapterNumber] = true;
        g.Save(); //Save data
        //global.GoToScene(0); //Go to title
    }
    #endregion private methods
}
