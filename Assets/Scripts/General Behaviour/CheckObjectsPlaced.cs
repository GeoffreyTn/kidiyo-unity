﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Checks wether all the DragAndDrop Pieces have been placed
/// </summary>
public class CheckObjectsPlaced : MonoBehaviour
{
    #region public variables
    /// <summary>
    /// All the pieces that should be dragged and drop to a particular spot
    /// </summary>
    public List<GameObject> DragAndDropPieces;
    #endregion public variables
    /// <summary>
    /// Used to continue the story if all pieces have been placed
    /// </summary>
    [SerializeField]
    private StoryProgressionManager storyManager;
    /// <summary>
    /// The nextObject to be activated
    /// </summary>
    [SerializeField]
    private GameObject NextObject;
    /// <summary>
    /// Time before the story will be advanced
    /// </summary>
    private float WaitTime = 1;

    #region events and delegates
    /// <summary>
    /// Delegate for the extra Invokable Event
    /// </summary>
    public delegate void InvokableEventAction();
    /// <summary>
    /// The extra event that will happen when the button is pressed
    /// </summary>
    public InvokableEventAction InvokableEvent;
    #endregion events and delegates

    #region public variables
    /// <summary>
    /// List of extra events that can be executed when all object has been placed
    /// </summary>
    public enum ExtraEvents
    {
        None, MoveCameraX, ActivateObject, ChangeBackground, FadeOut, MoveObject
    }

    [Header("Events that need to be invoked at a special time, i.e. after the fade effect is over.")]
    public ExtraEvents[] extraEvents;

    [Header("Id of the chapter that should get accessed onClick. Should be the same as actual chapter.")]
    public int ChapterNumber;

    [Header("Image to be used for fading effects")]
    public Image FadeImage;
    public float FadeTime = 1f;

    [HideInInspector] //Unused
    public float CamMoveXDist = 40f;

    public GameObject[] ObjectsToBeToggled, ObjectsToBeMoved, ObjectsToBeFadedOut, TargetObjects;

    public Sprite NewBackground;
    #endregion public variables

    /// <summary>
    /// Method that checks if the last piece that was placed was the last one.
    /// If this is true it will continue the story after a set amount of time
    /// </summary>
    public void CheckPieces()
    {
        if (AllPiecesPlaced())
            if (storyManager != null)
            {
                Invoke("ContinueStory", WaitTime);
            }
    }

    public void ContinueStory()
    {
        if (NextObject != null)
        {
            storyManager.AdvanceStory();
            NextObject.SetActive(true);
            gameObject.SetActive(false);
            //Make sure that the Invokable Event is empty so that next time there are no duplicate events
            EmptyInvokableEvent();
        }
    }

    /// <summary>
    /// Checks if all pieces have been placed if this is true it will add subscribers to the
    /// Invokable event and execute it here.
    /// </summary>
    /// <returns></returns>
    public bool AllPiecesPlaced()
    {
        foreach (var item in DragAndDropPieces)
        {
            if (item.GetComponent<DragAndDrop>().PiecePlaced == false)
                return false;
        }

        SetUpExtraEvent();

        if (InvokableEvent != null)
            InvokableEvent.Invoke();

        return true;
    }

    /// <summary>
    /// Unsubscribes all events from the InvokableEvent
    /// To make sure events/methods arent called more times than needed
    /// </summary>
    private void EmptyInvokableEvent()
    {
        //Unsubsribe all events
        //This is needed or else all events will be called more times than needed which causes huge memory leaks and bugs
        if (InvokableEvent!=null)
        {
            foreach (Delegate d in InvokableEvent.GetInvocationList())
            {
                InvokableEvent -= (InvokableEventAction)d;
            }
        }
    }

    /// <summary>
    /// Checks the array of ExtraEvents and subscribes the related methods to the InvokableEvent
    /// </summary>
    private void SetUpExtraEvent()
    {
        var gem = GlobalEventMethods.Instance;
        foreach (var extra in extraEvents)
        {
            switch (extra)
            {
                case ExtraEvents.MoveCameraX:
                    gem.MoveCameraDistanceX = CamMoveXDist;
                    InvokableEvent += gem.MoveCameraX;
                    break;
                case ExtraEvents.ActivateObject:
                    gem.ObjectsToBeToggled = ObjectsToBeToggled;
                    InvokableEvent += gem.ToggleEventObjectActivation;
                    break;
                case ExtraEvents.ChangeBackground:
                    gem.Background = NewBackground;
                    InvokableEvent += gem.ChangeBackground;
                    break;
                case ExtraEvents.FadeOut:
                    gem.ObjectsToBeFadedOut = ObjectsToBeFadedOut;
                    InvokableEvent += gem.FadeObjectOut;
                    break;
                case ExtraEvents.MoveObject:
                    gem.ObjectsToBeMoved = ObjectsToBeMoved;
                    gem.TargetObjects = TargetObjects;
                    InvokableEvent += gem.MoveObjects;
                    break;
                default:
                    break;
            }
        }
    }
}
