﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Script that handles the Washing minigame
/// </summary>
public class CleanBuddy : MonoBehaviour, IDragHandler, IEndDragHandler
{
    #region public variables
    public GameObject[] Dirtiness;
    public GameObject Bubbles, NextObject, NextObject2, PrevObject; //should be an array instead of nextObject2!!! TODO
    public Image Buddy;
    #endregion public variables

    #region private variables
    private float DragTime;
    private int DirtNumber;
    #endregion private variables

    #region monodevelop
    private void OnEnable()
    {
        Buddy.sprite = GetAnimalSprite();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (GlobalGameVariables.Instance.CanInteract)
            DragTime += Time.deltaTime;

        var a = Random.Range(0, 4);

        if (a == 1)
            if (IsCleaned())
                ContinueStory();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (DragTime > .1)
        {
            if (IsCleaned())
            {
                ContinueStory();
            }
        }
        DragTime = 0;
    }
    #endregion monodevelop

    #region public methods
    public bool IsCleaned()
    {
        if (DirtNumber < Dirtiness.Length)
        {
            Instantiate(Bubbles, this.transform);
            Dirtiness[DirtNumber].gameObject.SetActive(false);
            DirtNumber++;
            return false;
        }
        return true;
    }

    /// <summary>
    /// Gives the Buddy GameObject a sprite by checking the sprite of the chosen AnimalType scriptable Object
    /// </summary>
    /// <returns> Sprite of the chosen AnimalType object </returns>
    private Sprite GetAnimalSprite()
    {
        var ggv = GlobalGameVariables.Instance;
        if (ggv.AnimalType != null)
            return ggv.AnimalType.AnimalSprite;
        return GetComponent<Sprite>();
    }

    /// <summary>
    /// Advances the story and disables this object's parent
    /// </summary>
    public void ContinueStory()
    {
        StoryProgressionManager s = FindObjectOfType<StoryProgressionManager>();
        s.AdvanceStory();
        NextObject.SetActive(true);
        NextObject2.SetActive(true);
        PrevObject.SetActive(false);
    }
    #endregion public methods
}
