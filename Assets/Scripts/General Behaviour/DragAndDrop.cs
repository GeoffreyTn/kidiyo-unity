﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Script for dragging and object and dropping it onto a certain spot
/// </summary>
public class DragAndDrop : MonoBehaviour, IDragHandler, IEndDragHandler
{
    #region public variables
    public GameObject TargetPosition;
    public bool PiecePlaced;
    public AudioClip SoundOnPlaced;
    public Vector3 initPos;
    public Color initCol;
    #endregion public variables

    #region monodevelop
    private void Awake()
    {
        initPos = transform.position;
        initCol = GetComponent<Image>().color;
    }

    private void Update()
    {
        GetComponent<Image>().color = GlobalGameVariables.Instance.CanInteract ? initCol : Color.gray;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (transform.parent != TargetPosition.transform && GlobalGameVariables.Instance.CanInteract)
            transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (transform.parent != TargetPosition.transform && GlobalGameVariables.Instance.CanInteract)
            if (GameObject.FindGameObjectsWithTag("Room").Length==0)
                transform.position = initPos;
            else
                transform.localPosition = initPos;
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == TargetPosition)
        {
            transform.SetParent(TargetPosition.transform);
            transform.localPosition = Vector3.zero;
            PiecePlaced = true;

            if (SoundOnPlaced != null)
                SoundManager.PlaySound(SoundOnPlaced);
            GetComponentInParent<CheckObjectsPlaced>().CheckPieces();
        }
    }
    #endregion monodevelop
}
