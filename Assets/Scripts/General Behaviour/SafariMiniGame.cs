﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script that handles the safari mini-game
/// </summary>
public class SafariMiniGame : MonoBehaviour
{
    #region public variables
    public AudioSource source;

    public GameObject[] Bushes, ObjectsToBeToggled;

    public enum FindableAnimals
    {
        Frog/*, Peacock*/, Whale, Bee
    }
    public FindableAnimals WantedAnimal;

    //public int IndexOfAnimalToBeFound;
    /// <summary>
    /// Soundclip that introduces the animal to be found
    /// </summary>
    public AudioClip[] AnimalHints;

    public GameObject[] SoundSequences;
    #endregion public variables

    #region public methods
    /// <summary>
    /// Make sure that buttons are disabled during hints
    /// </summary>
    public void Update()
    {
        if (source.isPlaying)
        {
            foreach (var item in Bushes)
            {
                item.GetComponent<Button>().interactable = false;
            }
        }
        else
        {
            foreach (var item in Bushes)
            {
                item.GetComponent<Button>().interactable = true;
            }
        }
    }

    /// <summary>
    /// Move on to the next animal to find, when the player got the last animal right
    /// </summary>
    public void FindNextAnimal()
    {
        if (AnimalHints[(int)WantedAnimal] != null && source != null)
        {
            //source.clip = AnimalHints[(int)WantedAnimal];
            //source.Play();

            SoundSequences[(int)WantedAnimal].SetActive(true);
            SoundSequences[(int)WantedAnimal].GetComponent<SoundSequence>().StartDialogue();
        }

        if ((int)WantedAnimal < SoundSequences.Length-1)
            WantedAnimal = WantedAnimal + 1;
        else
            Invoke("ContinueStory", 10);

        
    }

    public void ContinueStory()
    {
        foreach (var obj in ObjectsToBeToggled)
        {
            obj.SetActive(!obj.activeInHierarchy);
        }
        StoryProgressionManager s = FindObjectOfType<StoryProgressionManager>();
        s.AdvanceStory();
    }
    #endregion public methods
}
