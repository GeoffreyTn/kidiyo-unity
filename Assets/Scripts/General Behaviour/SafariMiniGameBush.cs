﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Bush button in the Safari Minigame only reveals animal if the mini-game index is the same as this ID
/// </summary>
[RequireComponent(typeof(Button))]
public class SafariMiniGameBush : MonoBehaviour
{
    #region public variables
    //public int ID;
    public SafariMiniGame.FindableAnimals ContainedAnimal;
    public GameObject AnimalToBeRevealed, TargetLocationToMoveTo;
    #endregion public variables

    #region private variables
    private Button btn;
    private SafariMiniGame safariMiniGame;
    #endregion private variables

    #region monodevelop
    private void OnEnable()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(RevealAnimal);
        safariMiniGame = FindObjectOfType<SafariMiniGame>();
    }
    #endregion monodevelop

    #region public methods
    public void RevealAnimal()
    {
        if (safariMiniGame.WantedAnimal == ContainedAnimal && safariMiniGame.source.isPlaying==false)
        {
            AnimalToBeRevealed.SetActive(true);
            safariMiniGame.FindNextAnimal();
        }
    }
    #endregion public methods
}
