﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// Manages, saves and loads global game data
/// </summary>
public class GameDataManger : MonoBehaviour {

    #region public variables
    public static GameDataManger gameDataManger;

    //Data to be saved and loaded
    public DateTime PlayerBirthday;
    public string AnimalType;
    /// <summary>
    /// List of Chapters in the game.
    /// int = chapter number, bool is complete status
    /// </summary>
    public Dictionary<int, bool> Chapters = new Dictionary<int, bool>();
    //Data to be saved and loaded

    /// <summary>
    /// How many chapters the game currently has
    /// </summary>
    public int AmountOfChapters;
    /// <summary>
    /// The chapter the player is currently viewing
    /// </summary>
    public int CurrentchapterNumber;
    #endregion public variables

    #region monodevelop
    /// <summary>
    /// Setup this object so that there is only one instance of it.
    /// </summary>
    private void Awake()
    {
        if (gameDataManger!=null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            gameDataManger = this;
        }
    }

    /// <summary>
    /// Methods that have to be executed onStart
    /// </summary>
    private void Start()
    {
        SetupData();
    }
    #endregion monodevelop

    #region public methods
    /// <summary>
    /// Setup Save Data for first use
    /// </summary>
    public void SetupData()
    {
        //DeleteGameDate(); //only for testing
        if (File.Exists(Application.persistentDataPath + "/GameData.dat"))
        {
            Load();
        }
        else
        {
            //Make sure no presents are unlocked
            foreach (var item in Resources.LoadAll<PresentType>("Scriptable Objects/Presents"))
            {
                item.IsUnlocked = false;
            }

            PlayerBirthday = DateTime.Now;
            AnimalType = "None";

            for (int i = 0; i < AmountOfChapters; i++)
            {
                Chapters.Add(i, false);
            }
            //Saves the just made playerData
            Save();
            //Load the data that was just made and saved
            Load();
        }

        //Set animal type
        var ggv = GlobalGameVariables.Instance;
        AnimalType[] types = Resources.LoadAll<AnimalType>("Scriptable Objects/Buddies");
        foreach (var type in types)
        {
            if (type.name == AnimalType)
                ggv.AnimalType = type;
        }
    }

    /// <summary>
    /// Saves the player, animal and chapter data to the GameData file
    /// </summary>
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GameData.dat");

        GameData data = new GameData();

        //Save current data to file
        data.PlayerBirthday = PlayerBirthday;
        data.AnimalType = AnimalType;
        data.Chapters = Chapters;

        bf.Serialize(file, data);
        file.Close();
    }

    /// <summary>
    /// Loads the player, animal and chapter data from the GameData file
    /// </summary>
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/GameData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameData.dat", FileMode.Open);
            GameData data = (GameData)bf.Deserialize(file);
            file.Close();

            //Load file data to class
            PlayerBirthday = data.PlayerBirthday;
            AnimalType = data.AnimalType;
            Chapters = data.Chapters;
        }
    }

    /// <summary>
    /// Deletes the Player, Buddy and Chapter data
    /// </summary>
    public void DeleteGameDate()
    {
        if (File.Exists(Application.persistentDataPath + "/GameData.dat"))
        {
            File.Delete(Application.persistentDataPath + "/GameData.dat");
        }
    }
    #endregion public methods
}

[Serializable]
class GameData
{
    public DateTime PlayerBirthday;
    public string AnimalType;
    public Dictionary<int, bool> Chapters = new Dictionary<int, bool>();
}