﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class containing all eventmethods. These methods are called from an event (InvokableEvent) from other classes
/// </summary>
public class GlobalEventMethods : MonoBehaviour {
    
    #region public static variables
    public static GlobalEventMethods Instance { get; private set; }
    #endregion public static variables

    #region private variables
    /// <summary>
    /// Used to call the fade ienumerator instruction again
    /// </summary>
    private YieldInstruction fadeInstruction = new YieldInstruction();
    #endregion private variables

    #region variables
    private Camera cam;
    [HideInInspector]
    public float MoveCameraDistanceX = 50;
    [HideInInspector]
    public GameObject[] ObjectsToBeToggled, ObjectsToBeMoved, ObjectsToBeFadedOut, TargetObjects;
    [HideInInspector]
    public Sprite Background;
    [SerializeField]
    private GameObject Back;
    #endregion variables

    #region monodevelop
    /// <summary>
    /// Setup this object so that there is only one instance of it.
    /// </summary>
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    #endregion monodevelop

    #region public methods
    public void MoveCameraX()
    {
        cam = FindObjectOfType<Camera>();
        Vector3 nPos = new Vector3(cam.transform.position.x + MoveCameraDistanceX, cam.transform.position.y, cam.transform.position.z);
        cam.transform.position = nPos;
    }

    public void ChangeBackground()
    {
        if (Back != null)
            Back.GetComponent<Image>().sprite = Background;
        else
        {
            Back = GameObject.FindGameObjectsWithTag("Back")[0];
            Back.GetComponent<Image>().sprite = Background;
        }
    }

    /// <summary>
    /// Activates or Deactivates the object to be toggled
    /// </summary>
    public void ToggleEventObjectActivation()
    {
        int i = 0;
        foreach (var obj in ObjectsToBeToggled)
        {   i++;
            if (i <= ObjectsToBeToggled.Length)
                obj.SetActive(!obj.activeInHierarchy);
        }
    }

    /// <summary>
    /// Fades all objects out from the given array.
    /// After its done fading the objects will be destroyed
    /// </summary>
    public void FadeObjectOut()
    {
        var ggm = GlobalGameMethods.Instance;
        var time = 1f;

        int i = 0;
        foreach (var obj in ObjectsToBeFadedOut)
        {
            i++;
            if (i <= ObjectsToBeFadedOut.Length)
                StartCoroutine(ggm.FadeOut(obj.GetComponent<Image>(), time, true, obj, null)); 
        }
    }

    /// <summary>
    /// Moves all objects from the class array.
    /// The objects will move towards an item from the TargetObjects array. 
    /// It will move to the target with the same index
    /// </summary>
    public void MoveObjects()
    {
        var ggm = GlobalGameMethods.Instance;
        var time = 1f;

        StartCoroutine(ggm.MoveObjectsToTarget(time, ObjectsToBeMoved, TargetObjects));
    }
    #endregion public methods
}
