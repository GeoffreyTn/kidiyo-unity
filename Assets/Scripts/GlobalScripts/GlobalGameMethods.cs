﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Singleton class for accessing global methods.
/// </summary>
public class GlobalGameMethods : MonoBehaviour
{
    #region public static variables
    public static GlobalGameMethods Instance { get; private set; }
    #endregion public static variables

    #region private variables
    /// <summary>
    /// Used to call the fade ienumerator instruction again
    /// </summary>
    private YieldInstruction fadeInstruction = new YieldInstruction();
    #endregion private variables

    #region public variables
    /// <summary>
    /// The Directions an Object can move in 
    /// For the move method
    /// </summary>
    public enum MoveDirectons
    {
        Up, Down, Left, Right
    }
    #endregion public variables

    #region monodevelop
    private void Awake()
    {
        //Used to access the class but it shouldnt be a singleton.
        // Or it will be hard to find for objects
        Instance = this;
    }
    #endregion monodevelop

    #region public methods
    public void GoToScene(int ChapterNumber)
    {
        //Make sure that the game is not in freeze state onRestart
        GlobalGameVariables.Instance.CanInteract = true;

        var gdm = GameDataManger.gameDataManger;
        var ggv = GlobalGameVariables.Instance;
        gdm.CurrentchapterNumber = ChapterNumber;
        SceneManager.LoadScene(ggv.ChapterNames[ChapterNumber]);
    }

    /// <summary>
    /// Plays a sound from the Resources folder.
    /// for path use: GlobalGameVariables.Instance.NameOfTheSound
    /// </summary>
    /// <param name="path">GlobalGameVariables.Instance.NameOfTheSound</param>
    public void PlayResourceSound(string path)
    {
        var global = GlobalGameVariables.Instance;
        //SoundManager.PlaySound((AudioClip)Resources.Load(path));
    }

    /// <summary>
    /// Changes the size of an UI object to the TargetSize
    /// </summary>
    /// <param name="time"> How long should the resize take? In seconds</param>
    /// <param name="_TargetSize"> The new size of the object. Based on localScale </param>
    /// <param name="obj"> The object of which the size will change </param>
    /// <param name="shouldDestroy"> Should the object be destroyed when the size is changed? </param>
    /// <param name="invokableEventAction"> An extra event/method that will be executed when this method is done </param>
    /// <returns></returns>
    public IEnumerator ChangeSize(float time, float _TargetSize, GameObject obj, bool shouldDestroy,
        InteractableButton.InvokableEventAction invokableEventAction)
    {
        Vector3 InitialSize = obj.transform.localScale;
        Vector3 TargetSize = new Vector3(_TargetSize, _TargetSize, _TargetSize);

        float currentTime = 0.0f;

        do
        {
            obj.transform.localScale = Vector3.Lerp(InitialSize, TargetSize, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        InvokeExtraMethod(invokableEventAction);
        DestroyTheObject(shouldDestroy, obj);
    }

    /// <summary>
    /// Moves an Array of objects towards the Target object with the same index
    /// </summary>
    /// <param name="time"> How long should it take before the object reaches its target? In seconds </param>
    /// <param name="ObjectsToBeMoved"> Array of objects that will be moved towards their respective targets </param>
    /// <param name="TargetObjects"> Array of TargetObjects of which objects will move to </param>
    /// <returns> null </returns>
    public IEnumerator MoveObjectsToTarget(float time, GameObject[] ObjectsToBeMoved, GameObject[] TargetObjects)
    {
        //The initial position of all objects
        Vector3[] initObj = new Vector3[ObjectsToBeMoved.Length];

        int i = 0;

        foreach (var obj in ObjectsToBeMoved)
        {
            initObj[i] = obj.transform.position;
            i++;
        }

        float currentTime = 0.0f;

        do
        {
            int j = 0;

            foreach (var obj in ObjectsToBeMoved)
            {
                obj.transform.position = Vector3.Lerp(initObj[j], TargetObjects[j].transform.position, currentTime / time);
                j++;
            }
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);
    }

    /// <summary>
    /// Moves an array of objects into a single direction for an set amount of time.
    /// </summary>
    /// <param name="time"> How long the objects will move towards a single direction </param>
    /// <param name="ObjectsToBeMoved"> Array of object that will be moved </param>
    /// <param name="direction"> The direction these objects will be moved in </param>
    /// <param name="StartPositionY"> The position the objects will go to if the Y has reached its limit </param>
    /// <param name="EndPositionY"> the max height the objects can reach </param>
    /// <param name="StartPositionX"> The position the objects will go to if the X has reached its limit </param>
    /// <param name="EndPositionX"> the max width the objects can reach</param>
    /// <param name="speed"> How fast the objects will move into a certain position </param>
    /// <returns></returns>
    public IEnumerator MoveObjectsInDirection(float time, GameObject[] ObjectsToBeMoved, MoveDirectons direction,
        Vector3 StartPositionY, Vector3 EndPositionY, Vector3 StartPositionX, Vector3 EndPositionX, float speed)
    {
        float currentTime = 0.0f;

        do
        {
            int j = 0;

            foreach (var obj in ObjectsToBeMoved)
            {
                switch (direction)
                {
                    case MoveDirectons.Up:
                        obj.transform.Translate(new Vector3(0, speed));

                        if (obj.transform.position.y >= EndPositionY.y)
                        {
                            obj.transform.position = StartPositionY;
                        }
                        break;
                    case MoveDirectons.Down:
                        obj.transform.Translate(new Vector3(0, -speed));

                        if (obj.transform.position.y <= StartPositionY.y)
                        {
                            obj.transform.position = EndPositionY;
                        }
                        break;
                    case MoveDirectons.Left:
                        obj.transform.Translate(new Vector3(-speed, 0));

                        if (obj.transform.position.x <= StartPositionX.x)
                        {
                            obj.transform.position = EndPositionX;
                        }

                        break;
                    case MoveDirectons.Right:
                        obj.transform.Translate(new Vector3(speed, 0));

                        if (obj.transform.position.x >= EndPositionX.x)
                        {
                            obj.transform.position = StartPositionX;
                        }

                        break;
                    default:
                        break;
                }

                j++;
            }
            currentTime += Time.deltaTime;

            yield return null;
        } while (currentTime <= time);
    }

    /// <summary>
    /// Fades an image in and then fades it out.
    /// </summary>
    /// <param name="image"> The image that will be faded </param>
    /// <param name="fadeTime"> How long the fadeProcess will take in seconds </param>
    /// <param name="shouldDestroy"> Should the object be destroyed when this process is over? </param>
    /// <param name="obj"> The object that will be destroyed if ShouldDestroy is set to true</param>
    /// <param name="invokableEventAction"> Extra event/method that will be executed after this process is over </param>
    /// <returns></returns>
    public IEnumerator FadeInAndout(Image image, float fadeTime, bool shouldDestroy, GameObject obj,
        InteractableButton.InvokableEventAction invokableEventAction)
    {
        float elapsedTime = 0.0f;
        Color c = Color.white;
        if (image != null)
        {
            c = image.color;
            //revert the alpha to 0 if it is already at 1
            c.a = c.a > .9 ? 0 : c.a;
        }
        else
            yield return null;


        while (elapsedTime < fadeTime)
        {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            c.a = Mathf.Clamp01(elapsedTime / fadeTime);
            if (image != null)
                image.color = c;
            else
                yield return null;
        }
        InvokeExtraMethod(invokableEventAction);
        DestroyTheObject(shouldDestroy, obj);

        StartCoroutine(FadeOut(image, fadeTime, shouldDestroy, obj, null));
    }

    /// <summary>
    /// Fades an image out
    /// </summary>
    /// <param name="image"> Image that will be faded out </param>
    /// <param name="fadeTime"> Time it takes before the image is faded out in seconds </param>
    /// <param name="shouldDestroy"> Should the obj be destroyed when this process is over? </param>
    /// <param name="obj"> object that will be destroyed when ShouldDestroy is set to true </param>
    /// <param name="invokableEventAction"> Extra event/methdo that will be executed after this process is over </param>
    /// <returns></returns>
    public IEnumerator FadeOut(Image image, float fadeTime, bool shouldDestroy, GameObject obj,
        InteractableButton.InvokableEventAction invokableEventAction)
    {
        float elapsedTime = 0.0f;
        Color c = Color.white;
        if (image != null)
        {
            c = image.color;
            //revert the alpha to 0 if it is already at 1
            c.a = c.a > .9 ? 0 : c.a;
        }
        else
            yield return null;

        while (elapsedTime < fadeTime)
        {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            c.a = 1.0f - Mathf.Clamp01(elapsedTime / fadeTime);
            if (image != null)
                image.color = c;
            else
                yield return null;
        }

        InvokeExtraMethod(invokableEventAction);
        DestroyTheObject(shouldDestroy, obj);
    }

    /// <summary>
    /// Fades in an image
    /// </summary>
    /// <param name="image"> The image that will be faded in </param>
    /// <param name="fadeTime"> The time it will take before its faded in seconds </param>
    /// <param name="shouldDestroy"> Should the obj be destroyed when the process in done? </param>
    /// <param name="obj"> Object that will be destroyed if ShouldDestroyed is set to true </param>
    /// <param name="invokableEventAction"> Extra event/method that will be executed after the process is done </param>
    /// <returns></returns>
    public IEnumerator FadeIn(Image image, float fadeTime, bool shouldDestroy, GameObject obj,
        InteractableButton.InvokableEventAction invokableEventAction)
    {
        float elapsedTime = 0.0f;
        Color c = Color.white;
        if (image != null)
        {
            c = image.color;
            //revert the alpha to 0 if it is already at 1
            c.a = c.a > .9 ? 0 : c.a;
        }
        else
            yield return null;

        while (elapsedTime < fadeTime)
        {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            c.a = Mathf.Clamp01(elapsedTime / fadeTime);
            if (image != null)
                image.color = c;
            else
                yield return null;
        }

        InvokeExtraMethod(invokableEventAction);
        DestroyTheObject(shouldDestroy, obj);
        
    }
    #endregion public methods

    #region private methods
    /// <summary>
    /// Invokes the extra method from the interactable button
    /// </summary>
    /// <param name="invokableEventAction"></param>
    private void InvokeExtraMethod(InteractableButton.InvokableEventAction invokableEventAction)
    {
        //Invoke extra events from caller
        if (invokableEventAction != null)
            invokableEventAction.Invoke();
    }

    /// <summary>
    /// Destroys the object is ShouldDestroy is set to true.
    /// </summary>
    /// <param name="shouldDestroy"> Determines wether the obj will be destroyed </param>
    /// <param name="obj"> The object that will be destroyed </param>
    private void DestroyTheObject(bool shouldDestroy, GameObject obj)
    {
        //Destroy if need be
        if (shouldDestroy)
            Destroy(obj);
    }
    #endregion private methods
}
