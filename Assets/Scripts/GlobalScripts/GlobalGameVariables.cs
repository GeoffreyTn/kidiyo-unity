﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Singleton class for storing Game Variables used in several classes
/// </summary>
public class GlobalGameVariables : MonoBehaviour {

    #region public static variables
    public static GlobalGameVariables Instance { get; private set; }
    #endregion public static variables

    #region public variables
    /// <summary>
    /// Can the player interact with interactable objects?
    /// AKA disable check
    /// </summary>
    public bool CanInteract = true;
    public bool GameStarted;

    /// <summary>
    /// Current Buddy
    /// </summary>
    public AnimalType AnimalType;
    /// <summary>
    /// /What the buddy is currently wearing
    /// </summary>
    public PresentType Head, Body, Shoes, Toy;

    public Dictionary<int, string> ChapterNames = new Dictionary<int, string>();
    #endregion public variables

    #region private variables
    private Button StartButton;
    #endregion private variables

    #region monodevelop
    /// <summary>
    /// Setup this object so that there is only one instance of it.
    /// </summary>
    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        FillChapterNames();
    }

    /// <summary>
    /// Handles the Tommy Talking animation. Based on wether a Sound is playing.
    /// </summary>
    private void Update()
    {
        if (!CanInteract)
            Tommy.isTalking = true;
        else if (CanInteract && SoundManager.IsPlayingSound()==false)
            Tommy.isTalking = false;
        else if (CanInteract && SoundManager.IsPlayingSound() == true)
            Tommy.isTalking = true;
    }
    #endregion monodevelop

    #region public methods
    /// <summary>
    /// Toggle the gamestart bool so that the player doesn't have to click the button when he returns to the title.
    /// </summary>
    public void ToggleGameStart()
    {
        GameStarted = !GameStarted;
    }
    #endregion public methods

    #region private methods
    /// <summary>
    /// Here Level Names are filled. Be sure to add the Id which should correspond with the level #no.
    /// The name itself should be the same as the scenename!
    /// </summary>
    private void FillChapterNames()
    {
        ChapterNames.Add(0, "TitleScreen");
        ChapterNames.Add(1, "Chapter 1");
        ChapterNames.Add(2, "Chapter 2");
    }
    #endregion private methods
}
