﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes sure that all Puzzle Relatd objects are disabled when the puzzle minigame is done
/// </summary>
public class PuzzleSupport : MonoBehaviour {
    /// <summary>
    /// These objects made by Puzzle Maker light by name and should be deactivated when the puzzle is done
    /// </summary>
    [HideInInspector]
    public string[] PuzzleObjectsToBeToggled = new string[] 
    { "PuzzleP", "Piece0", "Piece1", "Piece2", "Piece3", "Piece4", "Piece5", "Piece6" , "Piece7" , "Piece8", "ActualPuzzleImage" };

    /// <summary>
    /// Toggle the status of all Puzzle related objects
    /// </summary>
    public void TogglePuzzleObjects()
    {
        foreach (var item in PuzzleObjectsToBeToggled)
        {
            GameObject obj = GameObject.Find(item);
            if (obj != null)
                obj.SetActive(!obj.activeInHierarchy);
        }
    }
}
