﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AnimalType", menuName = "Scriptable Objects/Objects/Animal")]
public class AnimalType : ScriptableObject {

    public string AnimalName;
    public Sprite AnimalSprite;

    public Vector3 HeadPosition;

    public Vector3 BodyPosition;

    public Vector3 Feet1Position, Feet2Position;

    public Vector3 ToyPosition;
}
