﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : Interactable {

    #region private variables
    [SerializeField]
    private float VerticalSpeed = 5f, HorizontalSpeed = 8f, MaxHeight = 2f;
    private Vector3 startPosition;
    private bool StartMoving;
    #endregion private variables

    #region monodevelop
    private void Update()
    {
        if (StartMoving)
        {
            MoveBubble();
        }
        startPosition = transform.position;
    }
    #endregion monodevelop

    #region public methods
    public override void Interaction()
    {
        base.Interaction();
        //Toggle Movement
        StartMoving = !StartMoving;
    }
    #endregion public methods

    #region private methods
    private void MoveBubble()
    {
        var t = transform.position;

        transform.position = new Vector3
            (t.x += HorizontalSpeed * Time.deltaTime, 
            startPosition.y + Mathf.Sin(Time.time * VerticalSpeed), 
            t.y);
    }
    #endregion private methods
}
