﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script for the Buddy object in game
/// </summary>
[RequireComponent(typeof(Image))]
public class Buddy : MonoBehaviour {

    #region public variables
    public enum Size
    {
        Big, Small
    }
    public Size size;
    #endregion public variables

    #region private variables
    private GameObject HeadItem, BodyItem, FeetItem1, FeetItem2, ToyItem;
    private bool ItemsCreated;
    #endregion private variables

    #region monodevelop
    private void OnEnable()
    {
        if(!ItemsCreated)
            CreateItems();
        this.GetComponent<Image>().sprite = GetAnimalSprite();
        if (gameObject.GetComponent<BuddyBehaviour>() == null)
            this.gameObject.AddComponent<BuddyBehaviour>(); //add the behaviour script automatically
    }

    private void Update()
    {
        ManageItems();
    }
    #endregion monodevelop

    #region private methods
    /// <summary>
    /// Gives the Buddy GameObject a sprite by checking the sprite of the chosen AnimalType scriptable Object
    /// </summary>
    /// <returns> Sprite of the chosen AnimalType object </returns>
    private Sprite GetAnimalSprite()
    {
        var ggv = GlobalGameVariables.Instance;
        if (ggv.AnimalType!=null)
            return ggv.AnimalType.AnimalSprite;
        return GetComponent<Sprite>();
    }

    /// <summary>
    /// Creates the itemholder GameObjects used for giving the Buddy clothes
    /// </summary>
    private void CreateItems()
    {
        ItemsCreated = true;
        HeadItem = new GameObject("HeadItem");
        BodyItem = new GameObject("BodyItem");
        FeetItem1 = new GameObject("FeetItem1");
        FeetItem2 = new GameObject("FeetItem2");
        ToyItem = new GameObject("ToyItem");

        List<GameObject> Items = new List<GameObject>
        {
            FeetItem1,
            FeetItem2,
            HeadItem,
            BodyItem,
            ToyItem
        };

        foreach (var item in Items)
        {
            item.AddComponent<Image>();
            item.transform.SetParent(gameObject.transform);
        }
        
    }

    /// <summary>
    /// Makes sure that the animal items are at the right positions and sizes
    /// </summary>
    private void ManageItems()
    {
        var g = GlobalGameVariables.Instance;

        if (g.Head != null)
        {
            HeadItem.GetComponent<Image>().sprite = g.Head.Sprite;
            HeadItem.transform.localPosition = g.AnimalType.HeadPosition;
            SetItemScale(HeadItem);

            HeadItem.GetComponent<Image>().enabled = true;
        }
        else
        {
            HeadItem.GetComponent<Image>().enabled = false;
        }
        
        if (g.Body !=null)
        {
            BodyItem.GetComponent<Image>().sprite = g.Body.Sprite;
            BodyItem.transform.localPosition = g.AnimalType.BodyPosition;

            SetItemScale(BodyItem);

            BodyItem.GetComponent<Image>().enabled = true;
        }
        else
        {
            BodyItem.GetComponent<Image>().enabled = false;
        }

        if (g.Shoes !=null)
        {
            FeetItem1.GetComponent<Image>().sprite = g.Shoes.Sprite;
            FeetItem1.transform.localPosition = g.AnimalType.Feet1Position;

            SetItemScale(FeetItem1);

            FeetItem2.GetComponent<Image>().sprite = g.Shoes.Sprite;
            FeetItem2.transform.localPosition = g.AnimalType.Feet2Position;

            SetItemScale(FeetItem2);

            FeetItem1.GetComponent<Image>().enabled = true;
            FeetItem2.GetComponent<Image>().enabled = true;
        }
        else
        {
            FeetItem1.GetComponent<Image>().enabled = false;
            FeetItem2.GetComponent<Image>().enabled = false;
        }

        if (g.Toy != null)
        {
            ToyItem.GetComponent<Image>().sprite = g.Toy.Sprite;
            ToyItem.transform.localPosition = g.AnimalType.ToyPosition;

            SetItemScale(ToyItem);

            ToyItem.GetComponent<Image>().enabled = true;
        }
        else
        {
            ToyItem.GetComponent<Image>().enabled = false;
        }

    }

    /// <summary>
    /// Method for setting the localScale to two set values used for Buddy Items
    /// </summary>
    /// <param name="item"></param>
    private void SetItemScale(GameObject item)
    {
        item.transform.localScale =
                size == Size.Big ? new Vector3(.5f, .5f, .5f) : new Vector3(.4f, .4f, .4f);
    }
    #endregion private methods
}
