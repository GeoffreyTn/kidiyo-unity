﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for executing Buddy behaviour events
/// </summary>
public class BuddyBehaviour : MonoBehaviour
{
    #region public variables
    /// <summary>
    /// Actions the Buddy can perform
    /// </summary>
    public enum Actions
    {
        //Movement
        MoveLeft, MoveRight, Jump,
        //Events
        Celebrate, Inspect,
        //Other
        Reset
    }
    #endregion public variables

    #region private variables
    private static Vector3 initPos;
    private static bool CanJump, ReachedTarget;
    private static Vector3 Target;
    #endregion private variables

    #region monodevelop
    private void Start()
    {
        //StartCoroutine(PerformAction(1, Actions.Jump, 1));
        initPos = transform.position;
        //This would make them move around but atm thats not desirable
        //Invoke("PerformRandomAction", 1);
    }

    private void Update()
    {
        if (CanJump)
        {
            StartCoroutine(Jump());
            CanJump = false;
        }

        //Bool used to check wether the buddy is in a special room where he doesnt need to move
        bool roomsAreActive = GameObject.FindGameObjectsWithTag("Room").Length > 0;

        //Let the buddy move if he hasnt reached its target if there are any
        if (!ReachedTarget && TargetsAvailable() && !roomsAreActive)
        {
            CheckForInteractables();
            MoveTowardsTarget();
        }
    }
    #endregion monodevelop

    #region public methods
    /// <summary>
    /// Event for when the player has succesfully advanced the story
    /// </summary>
    public static void HappyForSucces()
    {
        CanJump = true;
    }

    /// <summary>
    /// Checks if there are any interactable objects the buddy can move towards
    /// </summary>
    public static void CheckForInteractables()
    {
        ReachedTarget = false;
        var interactableObjects = GameObject.FindGameObjectsWithTag("Int");

        if (interactableObjects != null && interactableObjects.Length > 0)
            Target = interactableObjects[0].transform.position;
        else
            Target = initPos;
    }
    #endregion public methods

    #region private methods
    private void PerformRandomAction()
    {
        var a = Random.Range(0, 6);
        print((Actions)a);
        StartCoroutine(PerformAction(5, (Actions)a, 1));
        Invoke("PerformRandomAction", 5);
    }

    /// <summary>
    /// Performs the given action
    /// </summary>
    /// <param name="timeToTake"></param>
    /// <param name="Action"></param>
    /// <param name="Speed"></param>
    /// <returns></returns>
    private IEnumerator PerformAction(float timeToTake, Actions Action, float Speed)
    {
        float elapsedTime = 0.0f;

        while (elapsedTime < timeToTake)
        {
            switch (Action)
            {
                case Actions.MoveLeft:
                case Actions.MoveRight:
                    MoveInDirection(Action, Speed);
                    break;
                case Actions.Jump:
                    StartCoroutine(Jump());
                    break;
                case Actions.Celebrate:
                    break;
                case Actions.Inspect:
                    break;
                case Actions.Reset:
                    ResetPosition();
                    break;
                default:
                    break;
            }
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        yield return null;
    }

    /// <summary>
    /// Lets the buddy move in one of 4 directions
    /// </summary>
    /// <param name="Direction"></param>
    /// <param name="Speed"></param>
    private void MoveInDirection(Actions Direction, float Speed)
    {
        switch (Direction)
        {
            case Actions.MoveLeft:
                gameObject.transform.Translate(-Speed, 0, 0);
                break;
            case Actions.MoveRight:
                gameObject.transform.Translate(Speed, 0, 0);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Lets the buddy make a small jump
    /// </summary>
    /// <returns></returns>
    private IEnumerator Jump()
    {
        float timeToTake = .5f;
        float Speed = 4f;

#if UNITY_EDITOR
        Speed = 4f;
#elif UNITY_ANDROID
        Speed = 16f;
#else
        Speed = 4f;
#endif

        float elapsedTime = 0.0f;

        while (elapsedTime < timeToTake / 2)
        {
            gameObject.transform.Translate(0, Speed, 0);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        while (elapsedTime >= timeToTake / 2 && elapsedTime < timeToTake)
        {
            gameObject.transform.Translate(0, -Speed, 0);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    /// <summary>
    /// Resets the position of the buddy
    /// </summary>
    public void ResetPosition()
    {
        gameObject.transform.position = initPos;
    }

    private void MoveTowardsTarget()
    {
        float speed = 200;

#if UNITY_EDITOR
        speed = 200;
#elif UNITY_ANDROID
        speed = 400f;
#else
        speed = 200;
#endif

        float step = speed * Time.deltaTime;

        ReachedTarget = BuddyHasReachedTarget();
        //Turn around
        if (Target.x > transform.position.x)
            transform.localScale = new Vector3(-1, 1, 1);
        else
            transform.localScale = new Vector3(1, 1, 1);


        if (Target == null || Target == Vector3.zero)
        {
            Target = initPos;
        }
        if (!ReachedTarget)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(Target.x, transform.position.y, transform.position.z), step);
        }
    }

    private bool BuddyHasReachedTarget()
    {
        float f;
        if (Target != null)
            f = Vector3.Distance(transform.position, Target);
        else
            f = 100;

        if (f <= 1)
        {
            return true;
        }
        else
            return false;
    }

    private bool TargetsAvailable()
    {
        var interactableObjects = GameObject.FindGameObjectsWithTag("Int");
        if (interactableObjects.Length > 0)
            return true;
        else
            return false;
    }
    #endregion private methods
}
