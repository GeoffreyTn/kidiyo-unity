﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Attach as component to an object to make it destory after a certain time
/// </summary>
public class DestoryAfterAWhile : MonoBehaviour
{
    #region private variables
    [SerializeField]
    private float DestroyInXSeconds;
    //[SerializeField]
    private bool ShouldFade;
    [SerializeField]
    private AudioClip ClipToBePlayed;
    #endregion private variables

    private void OnEnable()
    {
        Invoke("DestroyEvents", DestroyInXSeconds);
    }

    /// <summary>
    /// Everything that should happen OnDestroy
    /// </summary>
    private void DestroyEvents()
    {
        if (ClipToBePlayed != null)
            SoundManager.PlaySound(ClipToBePlayed);

        if (ShouldFade)
            GlobalGameMethods.Instance.FadeOut(GetComponent<Image>(), DestroyInXSeconds / 2, true, this.gameObject, null);
        else
            Destroy(this.gameObject);
    }
}
