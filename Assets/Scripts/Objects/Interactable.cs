﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Base class for interactable objects
/// </summary>
public class Interactable : MonoBehaviour
{
    #region public variables
    /// <summary>
    /// List of ways how an object can disappear
    /// </summary>
    public enum DisappearTypes
    {
        GrowAndDisappear, ShrinkAndDisappear, MoveOut, Grow, Shrink, Disappear
    }
    public DisappearTypes disappearType;

    [Header("Should this be disabled after it has been interacted with")]
    public bool Disable;
    #endregion public variables

    #region private variables
    /// <summary>
    /// Checks wether the interactable obj has a Button Component
    /// TODO should this be mandatory?
    /// </summary>
    private bool hasButton;
    private Button button;

    /// <summary>
    /// Should the obj be destroyed onInteract?
    /// </summary>
    [SerializeField]
    private bool ShouldDisappear;
    [SerializeField]
    [Header("Assign a AudioClip to enable the interaction to make a sound.")]
    private AudioClip PlaySingleSound;
    #endregion private variables

    #region monodevelop
    /// <summary>
    /// If the object has an Button add the Interact listener to it
    /// </summary>
    private void Start()
    {
        hasButton = GetComponent<Button>() != null ? true : false;
        if (hasButton)
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(OnClickInteract);
        }
    }

    /// <summary>
    /// Makes sure the player cant interact with this object if the game is in Freeze mode
    /// </summary>
    private void Update()
    {
        if (hasButton)
            GetComponent<Button>().interactable = GlobalGameVariables.Instance.CanInteract;
    }
    #endregion monodevelop

    #region public methods
    public void OnClickInteract()
    {
        Interaction();
    }

    /// <summary>
    /// What happens when there is an interaction with this object
    /// </summary>
    public virtual void Interaction()
    {
        PlayEventSound();

        if (ShouldDisappear)
        {
            ObjectDisappear();
        }

        if (Disable && hasButton)
        {
            GetComponent<Button>().enabled = false;
        }
    }
    #endregion public methods

    #region private methods
    private void PlayEventSound()
    {
        if (PlaySingleSound != null)
            SoundManager.PlaySound(PlaySingleSound);
    }

    /// <summary>
    /// What should happen when the Object disappears
    /// </summary>
    private void ObjectDisappear()
    {
        var gameMethods = GlobalGameMethods.Instance;

        switch (disappearType)
        {
            case DisappearTypes.GrowAndDisappear:
                StartCoroutine(gameMethods.ChangeSize(1, 2, this.gameObject, ShouldDisappear, null));
                StartCoroutine(gameMethods.FadeOut(button.gameObject.GetComponent<Image>(), .8f, 
                    ShouldDisappear, this.gameObject, null));
                break;
            case DisappearTypes.ShrinkAndDisappear:
                StartCoroutine(gameMethods.ChangeSize(1, 0, this.gameObject, ShouldDisappear, null));
                StartCoroutine(gameMethods.FadeOut(button.gameObject.GetComponent<Image>(), .8f, 
                    ShouldDisappear, this.gameObject, null));
                break;
            case DisappearTypes.MoveOut:
                break;
            case DisappearTypes.Grow:
                StartCoroutine(gameMethods.ChangeSize(1, 2, this.gameObject, ShouldDisappear, null));
                break;
            case DisappearTypes.Shrink:
                StartCoroutine(gameMethods.ChangeSize(1, 0, this.gameObject, ShouldDisappear, null));
                break;
            case DisappearTypes.Disappear:
                StartCoroutine(gameMethods.FadeOut(button.gameObject.GetComponent<Image>(), .8f, 
                    ShouldDisappear, this.gameObject, null));
                break;
            default:
                break;
        }
    }
    #endregion private methods
}
