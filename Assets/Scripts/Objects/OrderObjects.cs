﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Object that should be interacted with in the correct order
/// </summary>
public class OrderObjects : MonoBehaviour
{
    #region public variables
    public AudioClip CorrectSound, InCorrectSound;

    /// <summary>
    /// List of events that happen if the object has been succesfully activated (in right order)
    /// </summary>
    public enum InteractEvents
    {
        Destroy, Disable, ChangeColorToRandom
    }

    /// <summary>
    /// What event happens when the object has been succesfully activated (in right order) 
    /// </summary>
    public InteractEvents InteractEvent;
    #endregion public variables

    #region public methods
    /// <summary>
    /// Uses parent object to check if this was the next object in the orderlist
    /// </summary>
    public void OnInteract()
    {
        GetComponentInParent<InteractInOrder>().CheckInteractedObjects(gameObject, InteractEvent);
    }
    #endregion public methods
}
