﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Present Object on Present Screen
/// </summary>
public class Present : DragAndDrop
{
    #region public variables
    /// <summary>
    /// The presentType the present is based on
    /// </summary>
    public PresentType presentType;
    #endregion public variables

    #region private variables
    private Button Btn;
    /// <summary>
    /// Colors that indicate wether the player can interact with the present
    /// </summary>
    private Color col1, col2;
    /// <summary>
    /// Is the Buddy currently using/wearing this present?
    /// </summary>
    private bool IsWearing;
    #endregion private variables

    /// <summary>
    /// Adds all needed components, makes sure that the present's unlockstatus is correct and that its size is correct on all devices
    /// </summary>
    private void OnEnable()
    {
        //Btn = gameObject.GetComponent<Button>();
        //Btn.onClick.AddListener(IsSelected);

        TargetPosition = GameObject.Find("BuddyP");//FindObjectOfType<Buddy>().gameObject;

        //add needed components for drag and drop
        var bc = gameObject.GetComponent<BoxCollider2D>()==null ? gameObject.AddComponent<BoxCollider2D>()
            : gameObject.GetComponent<BoxCollider2D>();
        var rb = gameObject.GetComponent<Rigidbody2D>()==null ? gameObject.AddComponent<Rigidbody2D>()
            : gameObject.GetComponent<Rigidbody2D>();

        if (gameObject.GetComponent<InteractableButton>()==null)
            gameObject.AddComponent<InteractableButton>();

        rb.isKinematic = true;
        bc.size = new Vector2(100, 100);
        bc.isTrigger = true;

        //Show if can interact
        col1 = Color.white; col2 = Color.grey;
        col1.a = 1; col2.a = .0f;

        //Make sure the present is unlocked if the chapter was completed
        if (presentType != null)
        {
            if (!presentType.IsUnlocked)
            {
                var gdm = GameDataManger.gameDataManger;
                presentType.IsUnlocked = gdm.Chapters[presentType.Chapter];
            }
        }

        //Make sure the scale is right on all devices
#if UNITY_EDITOR
        this.transform.localScale = new Vector3(1, 1, 1);
#elif UNITY_ANDROID
        this.transform.localScale = new Vector3(1, 1, 1);
#else
        this.transform.localScale = new Vector3(1, 1, 1);
#endif
    }

    /// <summary>
    /// Makes sure that the player can only interact with the object when intended and that its shown visually
    /// </summary>
    private void Update()
    {
        this.GetComponent<Button>().enabled = presentType.IsUnlocked;
        this.GetComponent<Image>().color = presentType.IsUnlocked ? col1 : col2;
    }

    /// <summary>
    /// Toggle IsWearing status and "equips" buddy with this present and removes last item
    /// </summary>
    public void IsSelected()
    {
        var g = GlobalGameVariables.Instance;
        IsWearing = !IsWearing; //Little HACK here do this twice to remove last obj
        IsWearing = !IsWearing;

        switch (presentType.Present)
        {
            case PresentType.TypeOfPresent.Head:
                if (!IsWearing)
                    g.Head = presentType;
                else
                    g.Head = null;
                break;
            case PresentType.TypeOfPresent.Body:
                if (!IsWearing)
                    g.Body = presentType;
                else
                    g.Body = null;
                break;
            case PresentType.TypeOfPresent.Feet:
                if (!IsWearing)
                    g.Shoes = presentType;
                else
                    g.Shoes = null;
                break;
            case PresentType.TypeOfPresent.Toy:
                if (!IsWearing)
                    g.Toy = presentType;
                else
                    g.Toy = null;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Toggle isWearing status when the presentCollides with the Target
    /// </summary>
    /// <param name="collision"></param>
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (presentType.IsUnlocked)
            SoundManager.PlaySound(Resources.Load("Sounds/SFX/145440__soughtaftersounds__menu-dual-click") as AudioClip);
        else
            SoundManager.PlaySound(Resources.Load("Sounds/SFX/200469__callum-sharp279__menu-scroll-selection-sound") as AudioClip);

        if (collision.gameObject == TargetPosition)
        {
            PiecePlaced = true;

            if (SoundOnPlaced != null)
                SoundManager.PlaySound(SoundOnPlaced);
            if (presentType.IsUnlocked)
                IsSelected();
        }
    }
}
