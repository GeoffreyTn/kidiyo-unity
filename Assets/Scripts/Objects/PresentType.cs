﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PresentType", menuName = "Scriptable Objects/Objects/Present")]
public class PresentType : ScriptableObject {
    /// <summary>
    /// What kind of present this is
    /// </summary>
    public enum TypeOfPresent
    {
        Head, Body, Feet, Toy
    }

    public int ID;

    public TypeOfPresent Present;
    public Sprite Sprite;
    public string PresentName;

    public int Chapter;
    public bool IsUnlocked;
}
