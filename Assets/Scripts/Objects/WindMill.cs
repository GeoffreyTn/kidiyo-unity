﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Animates the windmill when the player makes a drag move as well as increases fire size.
/// </summary>
public class WindMill : MonoBehaviour, IDragHandler, IEndDragHandler
{
    #region private variables
    private float DragTime, spd = 150, spd2 = 300, RotateTime = 2f;
    private Color initCol;
    #endregion private variables

    #region public variables
    /// <summary>
    /// Amount of times the windMill will be used untill the story continues
    /// </summary>
    public int amount, maxAmount;
    /// <summary>
    /// Array of objects which activation will be toggled when the windMill event is over
    /// </summary>
    public GameObject[] ObjectsToToggle;
    public AudioClip SwipeSuccesSound;
    /// <summary>
    /// Location of the windEffects
    /// </summary>
    public GameObject SpawnLocation;

    public GameObject Cauldron;
    public Color[] CauldronHeatColors;
    #endregion public variables

    #region monodevelop
    private void Awake()
    {
        initCol = GetComponent<Image>().color;
    }

    private void Update()
    {
        GetComponent<Image>().color = GlobalGameVariables.Instance.CanInteract ? initCol : Color.black;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (GlobalGameVariables.Instance.CanInteract)
            DragTime += Time.deltaTime;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (GlobalGameVariables.Instance.CanInteract)
            SoundManager.PlaySound(SwipeSuccesSound);

        if (DragTime > .1)
        {
            MoveWindMill();
            if (HasMovedEnough())
            {
                ContinueStory();
            }
            else
            {
                ChangeCauldronColor(amount);
                return;
            }
        }
        DragTime = 0;
    }
    #endregion monodevelop

    #region public methods
    /// <summary>
    /// Checks wether the player has made the movement enough times
    /// </summary>
    /// <returns></returns>
    public bool HasMovedEnough()
    {
        amount += 1;
        if (amount >= maxAmount)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// What happens when the player succesfully moves the WindMill
    /// </summary>
    public void MoveWindMill()
    {
        //increase the size of the fire
        StartCoroutine(RotateFaster(RotateTime));
        Instantiate(Resources.Load("Prefabs/Objects/Wind"), SpawnLocation.transform);
    }

    public void ContinueStory()
    {
        foreach (var obj in ObjectsToToggle)
        {
            obj.SetActive(!obj.activeInHierarchy);
        }

        StoryProgressionManager s = FindObjectOfType<StoryProgressionManager>();
        s.AdvanceStory();
    }
    #endregion public methods

    #region private methods
    public IEnumerator RotateFaster(float time)
    {
        float currentTime = 0.0f;

        do
        {
            GetComponent<Rotate>().speed = spd2;
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        //return to original speed
        GetComponent<Rotate>().speed = spd;
    }

    /// <summary>
    /// Changes the color of the cauldron by getting a number and using that number to access the 
    /// correct color from the array
    /// </summary>
    /// <param name="no"> Number of amount of times the windmill has turned </param>
    private void ChangeCauldronColor(int no)
    {
        if (no < CauldronHeatColors.Length)
        if (Cauldron.GetComponent<Image>() != null)
            Cauldron.GetComponent<Image>().color =
                CauldronHeatColors[no];
    }
    #endregion private methods
}
