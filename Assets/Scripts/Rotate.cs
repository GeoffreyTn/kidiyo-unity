﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates the an object in the X-axis
/// </summary>
public class Rotate : MonoBehaviour {

    public float speed = -50;

	void Update () {
        transform.Rotate(0, 0, Time.deltaTime * speed);
    }
}
