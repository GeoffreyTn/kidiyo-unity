﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist : MonoBehaviour {

    private AudioSource audioSource;

    public AudioClip audio1; // Place Audio file
    public AudioClip audio2;

    // Lists
    public static List<AudioClip> PlayList = new List<AudioClip>();

    void Start()
    {
        PlayList.Add(audio1);
        PlayList.Add(audio2);
    }

    public void PlayThelist()
    {
        if (PlayList.Count >= 1)
        {
            StartCoroutine(AudioPlayList(PlayList[0].length));
        }
    }

    public IEnumerator AudioPlayList(float timer)
    {
        audioSource.clip = PlayList[0];
        audioSource.Play(); // Play first audio

        yield return new WaitForSeconds(timer + 1f);

        PlayList.RemoveAt(0); // Remove first audio

        if (PlayList.Count != 0)
        {
            StartCoroutine(AudioPlayList(PlayList[0].length)); // Play next audio
        }
    }
}
