﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enables that the game only makes a sound sometimes when the player places a piece
/// </summary>
public class PuzzlePlaySoundRandom : MonoBehaviour
{
    public void PlaySoundRandom()
    {
        var a = Random.Range(0, 3);

        if (a==2)
        SoundManager.PlaySound(Resources.Load<AudioClip>
            ("Sounds/Voice/VoiceOver/Chapter1/Project Kidiyo Voice Over-20180528T083010Z-001/Project Kidiyo Voice Over/P16"));
    }
}
