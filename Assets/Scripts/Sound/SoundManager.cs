﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO make this a singleton and persistent remove static
[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {

    #region private static variables
    private static AudioSource audioSource;
    #endregion private static variables

    #region mono develop
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    #endregion mono develop

    #region public static methods
    public static void PlaySound(AudioClip clip)
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }
        else
        {
            StopSound();
            PlaySound(clip);
        }

    }

    public static void StopSound()
    {
        if (audioSource != null)
            audioSource.Stop();
    }

    public static void PauseSound()
    {
        if (audioSource != null)
            audioSource.Pause();
    }

    public static bool IsPlayingSound()
    {
        return audioSource.isPlaying;
    }
    #endregion public static methods

}
