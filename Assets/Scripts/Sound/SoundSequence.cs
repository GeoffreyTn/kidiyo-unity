﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Play sounds in a sequence
/// </summary>
public class SoundSequence : MonoBehaviour {
    #region public variables
    public AudioClip[] SoundsToBePlayed;
    public float TimeBetweenSounds = 1;
    public bool DestroyWhenFinished = false;
    #endregion public variables

    #region private variables
    private int SoundNumber = 0;
    private GameObject SoundSource;
    private AudioSource source;
    #endregion private variables

    #region monodevelop
    private void OnEnable()
    {
        Setup();
    }
    #endregion monodevelop

    #region public methods
    /// <summary>
    /// Start playing the sounds
    /// </summary>
    public void StartDialogue()
    {
        //Make sure it starts at the start
        SoundNumber = 0;

        EventStartSound();
        StartCoroutine(PlaySounds());
    }

    /// <summary>
    /// What should happen before the sounds start playing
    /// </summary>
    public void EventStartSound()
    {
        GlobalGameVariables.Instance.CanInteract = false;
    }

    /// <summary>
    /// Stuff that should (or shouldnt) happen when sounds are playing
    /// </summary>
    public void EventDuringSound()
    {
        GlobalGameVariables.Instance.CanInteract = false;
    }

    /// <summary>
    /// What should happen after all sounds have played
    /// </summary>
    public void EventAfterSound()
    {
        //nothing yet
        GlobalGameVariables.Instance.CanInteract = true;
    }
    #endregion public methods

    #region private methods
    /// <summary>
    /// Play the list of sounds one after another. And then quit.
    /// </summary>
    /// <returns></returns>
    private IEnumerator PlaySounds()
    {
        SoundManager.StopSound(); //make sure other sounds have stopped

        //Debug.LogFormat("Play sound: {0}.", SoundsToBePlayed[SoundNumber]);
        source.clip = SoundsToBePlayed[SoundNumber]; //assign the correct clip
        source.Stop(); //temp
        source.Play();
        SoundNumber++; //set up the next sound
        //wait for the last clip to finish before playing the next one. And wait one more second
        if (SoundNumber < SoundsToBePlayed.Length) //only if the list hasnt finished yet
        {
            EventDuringSound();
            yield return new WaitForSeconds(SoundsToBePlayed[SoundNumber - 1].length + TimeBetweenSounds);
            StartCoroutine(PlaySounds());
        }
        else
        {
            SoundNumber--; //go back one entry to make sure it has stopped playing
            //if (SoundNumber!=0)
            //    yield return new WaitForSeconds(SoundsToBePlayed[SoundNumber - 1].length + TimeBetweenSounds);

            EventAfterSound();

            if (DestroyWhenFinished)
                Destroy(this.gameObject);
            //reset soundnumber for replays
            SoundNumber = 0;
            yield return null;
        }
        yield return null;
    }

    private void Setup()
    {
        //create the audiosource
        GameObject SoundSource = new GameObject("MyAudioSource");
        //Make it a child of this obj
        SoundSource.transform.parent = this.gameObject.transform;
        SoundSource.AddComponent<AudioSource>();
        source = SoundSource.GetComponent<AudioSource>();
    }
    #endregion private methods
}
