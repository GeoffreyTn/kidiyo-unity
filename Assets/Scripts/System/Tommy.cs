﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that handles Tommy's animation
/// </summary>
public class Tommy : MonoBehaviour {

    #region public variables
    public static bool isTalking, IsWaving;
    public Animator anim;
    public AudioClip Reminder;
    #endregion public variables

    public static float IdleTime, IdleMaxTime = 25;

    #region monodevelop
    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        StartWave();
    }

    private void Update()
    {
        anim.SetBool("Talking", isTalking);

        //Increase idle time if Tommy isnt talking
        if (!SoundManager.IsPlayingSound() && GlobalGameVariables.Instance.CanInteract)
            IdleTime += Time.deltaTime;

        if (IdleTime >= IdleMaxTime)
        {
            ResetIdle();
            PlayReminder();
        }
    }
    #endregion monodevelop

    #region public methods
    public void StartWave()
    {
        anim.SetTrigger("Wave");
    }

    public static void ResetIdle()
    {
        IdleTime = 0;
    }

    public void PlayReminder()
    {
        if (Reminder!=null)
        SoundManager.PlaySound(Reminder);
    }
    #endregion public methods
}
