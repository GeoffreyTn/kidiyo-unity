﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Script for testing behaviour
/// </summary>
public class Test : MonoBehaviour, IDragHandler, IEndDragHandler
{
    #region public variables
    public GameObject[] Dirtiness;
    public GameObject Bubbles;
    #endregion public variables

    #region private variables
    private float DragTime;
    private int DirtNumber;
    #endregion private variables

    #region monodevelop
    public void OnDrag(PointerEventData eventData)
    {
        //if (GlobalGameVariables.Instance.CanInteract)
            DragTime += Time.deltaTime;

        var a = Random.Range(0, 6);

        if (a==1)
            if (IsCleaned())
                print("all cleaned!");
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (DragTime > .1)
        {
            if (IsCleaned())
                print("all cleaned!");
        }
        DragTime = 0;
    }
    #endregion monodevelop

    #region public methods
    public bool IsCleaned()
    {
        if (DirtNumber<Dirtiness.Length)
        {
            Instantiate(Bubbles, this.transform);
            Dirtiness[DirtNumber].gameObject.SetActive(false);
            DirtNumber++;
            return false;
        }
        return true;
    }
    #endregion public methods
}