﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {
    #region public methods
    public virtual void Update()
    {
        FollowTheMouse();
    }
    
    public virtual void FollowTheMouse()
    {
        Vector2 pos;
        Canvas myCanvas = FindObjectOfType<Canvas>();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
        transform.position = myCanvas.transform.TransformPoint(pos);
    }
    #endregion public methods
}
