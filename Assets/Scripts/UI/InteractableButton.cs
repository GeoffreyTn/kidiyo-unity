﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Button that trigger interactable effects
/// </summary>
public class InteractableButton : Interactable
{
    #region events and delegates
    /// <summary>
    /// Delegate for the extra Invokable Event
    /// </summary>
    public delegate void InvokableEventAction();
    /// <summary>
    /// The extra event that will happen when the button is pressed
    /// </summary>
    public InvokableEventAction InvokableEvent;
    #endregion events and delegates

    #region public variables
    /// <summary>
    /// List of events that can happen when the button is clicked
    /// </summary>
    public enum ButtonEffect
    {
        None, GoToScene, FadeInAndOut
    }
    /// <summary>
    /// What kind of event will happen as soon as the button is clicked
    /// </summary>
    [Header("What happens when the button is pressed")]
    public ButtonEffect buttonEffect;

    /// <summary>
    /// List of extra events that can be executed when all object has been placed
    /// </summary>
    public enum ExtraEvents
    {
        None, MoveCameraX, ActivateObject, ChangeBackground, FadeOut, MoveObject
    }

    [Header("Events that need to be invoked at a special time, i.e. after the fade effect is over.")]
    public ExtraEvents[] extraEvents;

    [Header("Id of the chapter that should get accessed onClick. Should be the same as actual chapter.")]
    public int ChapterNumber;

    [Header("Image to be used for fading effects")]
    public Image FadeImage;
    public float FadeTime = 1f;

    [HideInInspector] //Unused
    public float CamMoveXDist = 40f;

    public GameObject[] ObjectsToBeToggled, ObjectsToBeMoved, ObjectsToBeFadedOut, TargetObjects;

    public Sprite NewBackground;
    #endregion public variables

    #region public methods
    /// <summary>
    /// Events that happen when the player interact with the button
    /// </summary>
    public override void Interaction()
    {
        base.Interaction();
        SetUpExtraEvent();

        var ggv = GlobalGameVariables.Instance;
        var gdm = GameDataManger.gameDataManger;
        var ggm = GlobalGameMethods.Instance;

        switch (buttonEffect)
        {
            case ButtonEffect.None:
                if (InvokableEvent != null)
                    InvokableEvent.Invoke(); //invoke extra events right away
                break;
            case ButtonEffect.GoToScene:
                gdm.CurrentchapterNumber = ChapterNumber;
                SceneManager.LoadScene(ggv.ChapterNames[ChapterNumber]);
                break;
            case ButtonEffect.FadeInAndOut:
                StartCoroutine(ggm.FadeInAndout(FadeImage, FadeTime, false, this.gameObject, InvokableEvent));
                break;
            default:
                break;
        }

        EmptyInvokableEvent();
    }
    #endregion public methods

    #region private methods
    /// <summary>
    /// Unsubscribes all events from the InvokableEvent
    /// To make sure events/methods arent called more times than needed
    /// </summary>
    private void EmptyInvokableEvent()
    {
        //Unsubsribe all events
        //This is needed or else all events will be called more times than needed which causes huge memory leaks and bugs
        if (InvokableEvent != null)
        {
            foreach (Delegate d in InvokableEvent.GetInvocationList())
            {
                InvokableEvent -= (InvokableEventAction)d;
            }
        } 
    }

    /// <summary>
    /// Checks the array of ExtraEvents and subscribes the related methods to the InvokableEvent
    /// </summary>
    private void SetUpExtraEvent()
    {
        var gem = GlobalEventMethods.Instance;
        if (extraEvents!=null)
        {
            foreach (var extra in extraEvents)
            {
                switch (extra)
                {
                    case ExtraEvents.MoveCameraX:
                        gem.MoveCameraDistanceX = CamMoveXDist;
                        InvokableEvent += gem.MoveCameraX;
                        break;
                case ExtraEvents.ActivateObject:
                        gem.ObjectsToBeToggled = ObjectsToBeToggled;
                        InvokableEvent += gem.ToggleEventObjectActivation;
                        break;
                case ExtraEvents.ChangeBackground:
                        gem.Background = NewBackground;
                        InvokableEvent += gem.ChangeBackground;
                        break;
                case ExtraEvents.FadeOut:
                        gem.ObjectsToBeFadedOut = ObjectsToBeFadedOut;
                        InvokableEvent += gem.FadeObjectOut;
                        break;
                case ExtraEvents.MoveObject:
                        gem.ObjectsToBeMoved = ObjectsToBeMoved;
                        gem.TargetObjects = TargetObjects;
                        InvokableEvent += gem.MoveObjects;
                        break;
                default:
                        break;
                }
            }
        }
    }
    #endregion private methods
}