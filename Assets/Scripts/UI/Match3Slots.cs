﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for the Match 3 slots mini-game, checks wether the displayed slots are the same as the goal slots
/// Moves the List of objects to direction in which the player has swiped
/// </summary>
public class Match3Slots : SwipeAction
{
    #region public variables
    public GameObject StartXPosition, StartYPosition, EndXPosition, EndYPosition;
    public GameObject[] ObjectsToMove;
    public float MoveSpeed=1f, MoveTime=1f;
    public bool AllowSwipeInAllDirection, AllowSwipeHorizontally, AllowSwipeVertically=true;
    #endregion public variables

    #region monodevelop
    private void OnEnable()
    {
#if UNITY_EDITOR
        MoveSpeed = MoveSpeed;
#elif UNITY_ANDROID
        MoveSpeed = 5;
#else
        MoveSpeed = MoveSpeed;
#endif
    }
    #endregion monodevelop

    #region public methods
    public override void HasSwipedUp()
    {
        if (AllowSwipeVertically || AllowSwipeInAllDirection)
        {
            var ggm = GlobalGameMethods.Instance;
            StartCoroutine(ggm.MoveObjectsInDirection(MoveTime, ObjectsToMove, GlobalGameMethods.MoveDirectons.Up,
                StartYPosition.transform.position, EndYPosition.transform.position,
                StartXPosition.transform.position, EndXPosition.transform.position, MoveSpeed));
        }   
    }

    public override void HasSwipedDown()
    {
        if (AllowSwipeVertically || AllowSwipeInAllDirection)
        {
            var ggm = GlobalGameMethods.Instance;
            StartCoroutine(ggm.MoveObjectsInDirection(MoveTime, ObjectsToMove, GlobalGameMethods.MoveDirectons.Down,
                StartYPosition.transform.position, EndYPosition.transform.position,
                StartXPosition.transform.position, EndXPosition.transform.position, MoveSpeed));
        }
    }

    public override void HasSwipedLeft()
    {
        if (AllowSwipeHorizontally || AllowSwipeInAllDirection)
        {
            var ggm = GlobalGameMethods.Instance;
            StartCoroutine(ggm.MoveObjectsInDirection(MoveTime, ObjectsToMove, GlobalGameMethods.MoveDirectons.Left,
                StartYPosition.transform.position, EndYPosition.transform.position,
                StartXPosition.transform.position, EndXPosition.transform.position, MoveSpeed));
        }
    }

    public override void HasSwipedRight()
    {
        if (AllowSwipeHorizontally || AllowSwipeInAllDirection)
        {
            var ggm = GlobalGameMethods.Instance;
            StartCoroutine(ggm.MoveObjectsInDirection(MoveTime, ObjectsToMove, GlobalGameMethods.MoveDirectons.Right,
                StartYPosition.transform.position, EndYPosition.transform.position,
                StartXPosition.transform.position, EndXPosition.transform.position, MoveSpeed));
        }
    }
    #endregion public methods
}
