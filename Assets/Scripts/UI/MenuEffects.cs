﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class with methods of effects Menu items can operate
/// </summary>
public class MenuEffects : MonoBehaviour {

    #region public variables
    public float FadeInOutTime = .5f, SizeChangeTime = 1f, TargetIncreaseSize = 2f, TargetDecreaseSize = 0f;

    /// <summary>
    /// List of ways how the menuItem can be shown to be activated
    /// </summary>
    public enum ActivateType
    {
        None, FadeIn, FadeOut, Grow, Shrink,
        FadeAndShrink, FadeAndGrow, FadeInAndGrow, FadeInAndShrink
    }
    public ActivateType activateType;
    public ActivateType deactivateType;
    #endregion public variables

    #region private variables
    private bool HasBeenActivated;
    #endregion private variables

    #region private methods
    /// <summary>
    /// What happens when the menuItemactivates
    /// </summary>
    private void OnEnable()
    {
        if (!HasBeenActivated)
            PerformActivationOperations(activateType);
    }

    /// <summary>
    /// Performs the chosen activation event for the menuItem
    /// </summary>
    /// <param name="a"> The activationType that will be executed </param>
    private void PerformActivationOperations(ActivateType a)
    {
        HasBeenActivated = true;
        switch (a)
        {
            case ActivateType.None:
                break;
            case ActivateType.FadeIn:
                StartCoroutine(GlobalGameMethods.Instance.FadeIn
                    (this.gameObject.GetComponent<Image>(), FadeInOutTime, false, this.gameObject, null));
                break;
            case ActivateType.FadeOut:
                StartCoroutine(GlobalGameMethods.Instance.FadeOut
                    (this.gameObject.GetComponent<Image>(), FadeInOutTime, false, this.gameObject, null));
                break;
            case ActivateType.Grow:
                StartCoroutine(GlobalGameMethods.Instance.ChangeSize
                    (SizeChangeTime, TargetIncreaseSize, this.gameObject, false, null));
                break;
            case ActivateType.Shrink:
                StartCoroutine(GlobalGameMethods.Instance.ChangeSize
                    (SizeChangeTime, TargetDecreaseSize, this.gameObject, false, null));
                break;
            case ActivateType.FadeAndShrink:
                StartCoroutine(GlobalGameMethods.Instance.FadeOut
                    (this.gameObject.GetComponent<Image>(), FadeInOutTime, false, this.gameObject, null));
                StartCoroutine(GlobalGameMethods.Instance.ChangeSize
                    (SizeChangeTime, TargetDecreaseSize, this.gameObject, false, null));
                break;
            case ActivateType.FadeAndGrow:
                StartCoroutine(GlobalGameMethods.Instance.FadeOut
                    (this.gameObject.GetComponent<Image>(), FadeInOutTime, false, this.gameObject, null));
                StartCoroutine(GlobalGameMethods.Instance.ChangeSize
                    (SizeChangeTime, TargetIncreaseSize, this.gameObject, false, null));
                break;
            case ActivateType.FadeInAndGrow:
                StartCoroutine(GlobalGameMethods.Instance.FadeIn
                    (this.gameObject.GetComponent<Image>(), FadeInOutTime, false, this.gameObject, null));
                StartCoroutine(GlobalGameMethods.Instance.ChangeSize
                    (SizeChangeTime, TargetIncreaseSize, this.gameObject, false, null));
                break;
            case ActivateType.FadeInAndShrink:
                StartCoroutine(GlobalGameMethods.Instance.FadeIn
                    (this.gameObject.GetComponent<Image>(), FadeInOutTime, false, this.gameObject, null));
                StartCoroutine(GlobalGameMethods.Instance.ChangeSize
                    (SizeChangeTime, TargetDecreaseSize, this.gameObject, false, null));
                break;
            default:
                break;
        }
    }
    #endregion private methods
}
