﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This scripts loads in all unlocked presents and lets the player give the buddy presents
/// </summary>
public class PresentScreen : MonoBehaviour {
    public int PageNumber;
    public GameObject[] SpawnLocations;
    private bool CreatedItems;

    private void OnEnable()
    {
        int i = 0;
        if (!CreatedItems)
        {
            foreach (var item in Resources.LoadAll<PresentType>("Scriptable Objects/Presents"))
            {
                if (item.IsUnlocked) //new
                {
                    CreateItem(item, i);
                    i = i < SpawnLocations.Length ? i + 1 : i = 0;
                }
            }
        }
    }

    private void CreateItem(PresentType item, int i)
    {
        CreatedItems = true;
        GameObject obj = new GameObject(item.PresentName);
        obj.transform.SetParent(SpawnLocations[i].transform);//(gameObject.transform);
        obj.AddComponent<Image>().sprite = item.Sprite;
        obj.AddComponent<Button>();
        obj.AddComponent<Present>().presentType = item;
        obj.transform.position = SpawnLocations[i].transform.position;
    }

    /// <summary>
    /// Removes all items from Buddy
    /// </summary>
    public void RemoveAllItems()
    {
        var ggv = GlobalGameVariables.Instance;
        ggv.Head = null; ggv.Body = null; ggv.Shoes = null; ggv.Toy = null;
    }
}
