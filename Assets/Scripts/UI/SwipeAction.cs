﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for Swipe Interactions in 4 directions.
/// Source: http://blog.trsquarelab.com/2015/02/detecting-swipe-from-unity3d-c-scripts.html
/// </summary>
public class SwipeAction : MonoBehaviour
{
    #region private variables
    private const int mMessageWidth = 200;
    private const int mMessageHeight = 64;

    private readonly Vector2 mXAxis = new Vector2(1, 0);
    private readonly Vector2 mYAxis = new Vector2(0, 1);


    private int mMessageIndex = 0;

    // The angle range for detecting swipe
    private const float mAngleRange = 10;

    // To recognize as swipe user should at lease swipe for this many pixels
    private const float mMinSwipeDist = 5.0f;

    // To recognize as a swipe the velocity of the swipe
    // should be at least mMinVelocity
    // Reduce or increase to control the swipe speed
    private const float mMinVelocity = 50.0f;

    private Vector2 mStartPosition;
    private float mSwipeStartTime;
    #endregion private variables

    #region monodevelop
    void Update()
    {
        // Mouse button down, possible chance for a swipe
        if (Input.GetMouseButtonDown(0))
        {
            // Record start time and position
            mStartPosition = new Vector2(Input.mousePosition.x,
                                         Input.mousePosition.y);
            mSwipeStartTime = Time.time;
        }

        // Mouse button up, possible chance for a swipe
        if (Input.GetMouseButtonUp(0))
        {
            float deltaTime = Time.time - mSwipeStartTime;

            Vector2 endPosition = new Vector2(Input.mousePosition.x,
                                               Input.mousePosition.y);
            Vector2 swipeVector = endPosition - mStartPosition;

            float velocity = swipeVector.magnitude / deltaTime;

            if (velocity > mMinVelocity &&
                swipeVector.magnitude > mMinSwipeDist)
            {
                // if the swipe has enough velocity and enough distance

                swipeVector.Normalize();

                float angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                // Detect left and right swipe
                if (angleOfSwipe < mAngleRange)
                {
                    OnSwipeRight();
                }
                else if ((180.0f - angleOfSwipe) < mAngleRange)
                {
                    OnSwipeLeft();
                }
                else
                {
                    // Detect top and bottom swipe
                    angleOfSwipe = Vector2.Dot(swipeVector, mYAxis);
                    angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;
                    if (angleOfSwipe < mAngleRange)
                    {
                        OnSwipeTop();
                    }
                    else if ((180.0f - angleOfSwipe) < mAngleRange)
                    {
                        OnSwipeBottom();
                    }
                    else
                    {
                        // unimplemented
                    }
                }
            }
        }
    }
    #endregion monodevelop

    #region private methods
    private void OnSwipeLeft()
    {
        HasSwipedLeft();
    }

    private void OnSwipeRight()
    {
        HasSwipedRight();
    }

    private void OnSwipeTop()
    {
        HasSwipedUp();
    }

    private void OnSwipeBottom()
    {
        HasSwipedDown();
    }
    #endregion private methods

    #region public methods
    /// <summary>
    /// Methods that are executed when the player has swiped right
    /// </summary>
    public virtual void HasSwipedRight()
    {
        print("put your own method for right swipe here.");
    }

    /// <summary>
    /// Methods that are executed  when the player has swiped left
    /// </summary>
    public virtual void HasSwipedLeft()
    {
        print("put your own method for left swipe here.");
    }

    /// <summary>
    /// Methods that are executed when the player has swiped up
    /// </summary>
    public virtual void HasSwipedUp()
    {
        print("put your own method for up swipe here.");
    }

    /// <summary>
    /// Methods that are executed when the player has swiped down
    /// </summary>
    public virtual void HasSwipedDown()
    {
        print("put your own method for down swipe here.");
    }
    #endregion public methods
}
