﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// When this script is added to an gameObject as an component. You can add EventTrigger actions
/// to this object. Like mouseEnter etc.
/// </summary>
public class UIEventTriggerActions : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //Could make an enum with different hover actions like an question mark ?

    #region public methods
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Do some sparkle effect here... So that the
        //Maybe can play sound effect too
        //Using a OnPointer class maybe?
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Same as above
    }
    #endregion public methods
}
